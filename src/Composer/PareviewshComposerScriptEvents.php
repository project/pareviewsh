<?php

namespace pareviewsh\Composer;

use Composer\Script\Event;

/**
 * Composer script event callbacks of PAReview.sh.
 */
class PareviewshComposerScriptEvents {

  /**
   * Pre-install callback of the PAReview.sh composer package.
   *
   * @param \Composer\Script\Event $event
   *   Composer event.
   */
  public static function preInstall(Event $event) {
    // PareviewshComposerActions::deleteEnvironmentCfg(
    // $event->getComposer(), $event->getIO());
  }

  /**
   * Pre-update callback of the PAReview.sh composer package.
   *
   * @param \Composer\Script\Event $event
   *   Composer event.
   */
  public static function preUpdate(Event $event) {
    // PareviewshComposerActions::deleteEnvironmentCfg(
    // $event->getComposer(), $event->getIO());
  }

  /**
   * Post install callback of the PAReview.sh composer package.
   *
   * @param \Composer\Script\Event $event
   *   Composer event.
   */
  public static function postInstall(Event $event) {
    /** @var \Composer\Composer $composer */
    $composer = $event->getComposer();
    /** @var \Composer\IO\IOInterface $io */
    $io = $event->getIO();

    PareviewshComposerActions::installDependencies($composer, $io);
    PareviewshComposerActions::createEnvironmentCfg($composer, $io);
  }

  /**
   * Post update callback of the PAReview.sh composer package.
   *
   * @param \Composer\Script\Event $event
   *   Composer event.
   */
  public static function postUpdate(Event $event) {
    /** @var \Composer\Composer $composer */
    $composer = $event->getComposer();
    /** @var \Composer\IO\IOInterface $io */
    $io = $event->getIO();

    PareviewshComposerActions::installDependencies($composer, $io);
    PareviewshComposerActions::createEnvironmentCfg($composer, $io);
  }

}

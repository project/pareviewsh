<?php

namespace pareviewsh\Composer;

use Composer\Composer;
use Composer\Factory;
use Composer\IO\IOInterface;
use Composer\Util\Filesystem;
use Composer\Util\Platform;
use Composer\Util\ProcessExecutor;

/**
 * Description of PareviewshComposerActions.
 *
 * Author msteinitz.
 */
class PareviewshComposerActions {

  /**
   * Checks, whether a shell command is available or not.
   *
   * @param string $command
   *   Command to check for.
   * @param \Composer\Util\ProcessExecutor $executor
   *   [Optional] Process executor to run the check with. Defaults to
   *   NULL to initialize a new process executor.
   *
   * @return bool
   *   TRUE, if the command is available, FALSE otherwise.
   */
  protected static function checkCommand($command, $executor = NULL) {
    if (is_null($executor)) {
      $executor = new ProcessExecutor();
    }
    $check = (Platform::isWindows()) ? 'where' : 'command -v';

    $output = '';
    $return = $executor->execute("{$check} {$command}", $output);

    return $return == 0 && !empty($output);
  }

  /**
   * Install dependencies.
   *
   * This helper method will be called by our post install and update
   * callbacks to install ESLint.
   *
   * @param \Composer\Composer $composer
   *   Composer instance.
   * @param \Composer\IO\IOInterface $io
   *   Composer IO.
   */
  public static function installDependencies(Composer $composer, IOInterface $io) {
    // Collect required environment information.
    /** @var \Composer\Config $config */
    $config = $composer->getConfig();
    $is_root = $composer->getPackage()->getName() == 'drupal/pareviewsh';
    $root_path = ($is_root) ? realpath(dirname(Factory::getComposerFile())) : $config->get('vendor-dir') . '/drupal/pareviewsh';

    // Initialize process executor with given UI.
    $executor = new ProcessExecutor($io);
    $output = '';

    // Install ESLint using `npm` package manager.
    $io->write("Installing ESLint (this may take a while, please be patient):");
    // Whether `npm` is installed.
    if (self::checkCommand('npm', $executor)) {
      // Run ESLint install commend (install node packages).
      $return = $executor->execute("npm install --no-progress --no-package-lock", $output, $root_path);
      // Get any error messages.
      $errors = $executor->getErrorOutput();
      // Send output to UI.
      $io->write($output);
      // Whether ESLint was installed successfully.
      if ($return === 0 && file_exists($root_path . '/node_modules/.bin/eslint')) {
        $io->write('ok');
      }
      else {
        // Write any errors to the UI.
        $io->writeError($errors);
        // Throw an exception, as ESLint is a requirement for PAReview.sh.
        throw new \RuntimeException("Could not install \"ESLint\".");
      }
    }
    else {
      // Npm is not available, so we can't install ESLint.
      $io->writeError('Command "npm" not found.');
      // Throw an exception, as ESLint is a requirement for PAReview.sh.
      throw new \RuntimeException("Could not execute \"npm\" command. Make sure, Node.js and \"npm\" are installed and executable for your user!");
    }
  }

  /**
   * Delete environment configuration file.
   *
   * @param \Composer\Composer $composer
   *   Composer instance.
   * @param \Composer\IO\IOInterface $io
   *   Composer IO.
   */
  public static function deleteEnvironmentCfg(Composer $composer, IOInterface $io) {
    $project_root_path = realpath(dirname(Factory::getComposerFile()));
    /** @var \Composer\Config $config */
    $config = $composer->getConfig();
    $is_root = $composer->getPackage()->getName() == 'drupal/pareviewsh';

    // Write configuration.
    $config_file = ($is_root ? $project_root_path : "{$config->get('vendor-dir')}/drupal/pareviewsh") . '/pareview.config';
    $filesystem = new Filesystem(new ProcessExecutor($io));
    $filesystem->remove($config_file);
  }

  /**
   * Create environment configuration file.
   *
   * This helper method will be called by our post install and update
   * callbacks to create a configuration file for the current PAReview.sh
   * environment.
   *
   * @param \Composer\Composer $composer
   *   Composer instance.
   * @param \Composer\IO\IOInterface $io
   *   Composer IO.
   */
  public static function createEnvironmentCfg(Composer $composer, IOInterface $io) {
    $project_root_path = realpath(dirname(Factory::getComposerFile()));
    /** @var \Composer\Config $config */
    $config = $composer->getConfig();
    $is_root = $composer->getPackage()->getName() == 'drupal/pareviewsh';

    // Write configuration.
    $config_file = ($is_root ? $project_root_path : "{$config->get('vendor-dir')}/drupal/pareviewsh") . '/pareview.config';
    $io->write("Writing PAReview.sh configuration to \"{$config_file}\":");
    $cfg_file = fopen($config_file, 'w');
    fwrite($cfg_file, "# Auto-generated PAReview.sh settings. Do not manually edit this file!\n# It will be overwritten on every composer install/update.\n");
    fwrite($cfg_file, "BIN_PHPCS=\"{$config->get('bin-dir')}/phpcs\"\n");
    fwrite($cfg_file, "PHPCS_DRUPAL=\"{$config->get('vendor-dir')}/drupal/coder/coder_sniffer/Drupal\"\n");
    fwrite($cfg_file, "PHPCS_DRUPALPRACTICE=\"{$config->get('vendor-dir')}/drupal/coder/coder_sniffer/DrupalPractice\"\n");
    fwrite($cfg_file, "PHPCS_DRUPALSECURE=\"{$config->get('vendor-dir')}/drupal/drupalsecure/DrupalSecure\"\n");
    if ($is_root) {
      fwrite($cfg_file, "BIN_ESLINT=\"{$project_root_path}/node_modules/.bin/eslint\"\n");
    }
    else {
      fwrite($cfg_file, "BIN_ESLINT=\"{$config->get('vendor-dir')}/drupal/pareviewsh/node_modules/.bin/eslint\"\n");
    }
    if ($is_root) {
      fwrite($cfg_file, "BIN_STYLELINT=\"{$project_root_path}/node_modules/.bin/stylelint\"\n");
    }
    else {
      fwrite($cfg_file, "BIN_STYLELINT=\"{$config->get('vendor-dir')}/drupal/pareviewsh/node_modules/.bin/stylelint\"\n");
    }
    if ($is_root) {
      fwrite($cfg_file, "BIN_CSPELL=\"{$project_root_path}/node_modules/.bin/cspell\"\n");
    }
    else {
      fwrite($cfg_file, "BIN_CSPELL=\"{$config->get('vendor-dir')}/drupal/pareviewsh/node_modules/.bin/cspell\"\n");
    }
    fwrite($cfg_file, "BIN_PHPSTAN=\"{$config->get('vendor-dir')}/phpstan/phpstan/phpstan\"\n");
    fclose($cfg_file);
    $io->write('ok');
  }

}

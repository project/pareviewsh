<?php

namespace pareviewsh\Composer;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;

/**
 * PareviewshComposerActions class.
 */
class PareviewshComposerInstallerPlugin implements PluginInterface, EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public function activate(Composer $composer, IOInterface $io) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ScriptEvents::PRE_INSTALL_CMD => 'preInstall',
      ScriptEvents::PRE_UPDATE_CMD => 'preUpdate',
      ScriptEvents::POST_INSTALL_CMD => 'postInstall',
      ScriptEvents::POST_UPDATE_CMD => 'postUpdate',
    ];
  }

  /**
   * Pre-install event for deleting environment configuration.
   *
   * @param \Composer\Script\Event $event
   *   Script event.
   */
  public function preInstall(Event $event) {
    // PareviewshComposerActions::deleteEnvironmentCfg(
    // $event->getComposer(), $event->getIO());
  }

  /**
   * Pre-update event for deleting environment configuration.
   *
   * @param \Composer\Script\Event $event
   *   Script event.
   */
  public function preUpdate(Event $event) {
    // PareviewshComposerActions::deleteEnvironmentCfg(
    // $event->getComposer(), $event->getIO());
  }

  /**
   * Post install event for installing dependencies and saving config.
   *
   * @param \Composer\Script\Event $event
   *   Script event.
   */
  public function postInstall(Event $event) {
    /** @var \Composer\Composer $composer */
    $composer = $event->getComposer();
    /** @var \Composer\IO\IOInterface $io */
    $io = $event->getIO();

    PareviewshComposerActions::installDependencies($composer, $io);
    PareviewshComposerActions::createEnvironmentCfg($composer, $io);
  }

  /**
   * Post update event for installing dependencies and saving config.
   *
   * @param \Composer\Script\Event $event
   *   Script event.
   */
  public function postUpdate(Event $event) {
    /** @var \Composer\Composer $composer */
    $composer = $event->getComposer();
    /** @var \Composer\IO\IOInterface $io */
    $io = $event->getIO();

    PareviewshComposerActions::installDependencies($composer, $io);
    PareviewshComposerActions::createEnvironmentCfg($composer, $io);
  }

}

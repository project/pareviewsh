# CONTENTS OF THIS FILE

 * Introduction
 * Installation
 * Configuration
 * Usage
 * Maintainers


## INTRODUCTION

PAReview.sh is a command line script to automatically review Drupal.org
project applications. It takes a Git repository URL as argument, clones
the code in a pareview_temp folder and runs some checks.

Alternatively it takes a path to a module/theme project and checks that.

The output is suitable for a comment in the Project Applications issue queue.

 * For a full description of the module, visit the project page:
   <https://drupal.org/project/pareviewsh>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://drupal.org/project/issues/pareviewsh>


## INSTALLATION

### Tested on Ubuntu 22.04
 *  sudo apt install git php8.1-cli php8.1-mbstring
    php8.1-simplexml php8.1-curl npm
 *  Install composer globally (https://getcomposer.org/download)
 *  Download and extract pareviewsh. Go to pareviewsh folder
 *  Run composer install


## CONFIGURATION

If you installed PAReview.sh using one of the above methods, there should be
no need for any further configuration.


## USAGE

PAReview.sh is a shell executable, so you can run it on the command
line interface:

    $> pareview.sh GIT-URL [BRANCH]
    $> pareview.sh DIR-PATH

Examples:

    $> pareview.sh https://git.drupalcode.org/project/media_gallery.git
    $> pareview.sh https://git.drupalcode.org/project/media_gallery.git 2.0.x
    $> pareview.sh sites/all/modules/media_gallery
    $> pareview.sh web/modules/custom/media_gallery


## MAINTAINERS

 * Andrei Ivnitskii - <https://www.drupal.org/u/ivnish>
 * Klaus Purer - <https://www.drupal.org/u/klausi>
